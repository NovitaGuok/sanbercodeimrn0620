/**
 * Nomor 1
 */
function arrayToObject(arr) {
  var now = new Date();
  var thisYear = now.getFullYear(); // 2020 (tahun sekarang)

  arr.forEach((elm) => {
    if (elm[3] == null || thisYear - elm[3] < 0) {
      var age = 'Invalid Birth Year';
    } else {
      var age = thisYear - elm[3];
    }
    console.log({
      firstName: elm[0],
      lastName: elm[1],
      gender: elm[2],
      age: age,
    });
  });
}

// Driver Code
var people = [
  ['Bruce', 'Banner', 'male', 1975],
  ['Natasha', 'Romanoff', 'female'],
];

arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ['Tony', 'Stark', 'male', 1980],
  ['Pepper', 'Pots', 'female', 2023],
];

arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]); // ""
console.log('*******************************');

/**
 * Nomor 2
 */
function search(arr, key) {
  i = 0;
  while (i < 5 && arr[i] != key) {
    i++;
  }
  if (arr[i] == key) {
    return true;
  } else {
    return false;
  }
}
function shoppingTime(memberId, money) {
  if (memberId == null || memberId == '') {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja';
  } else {
    if (money < 50000) {
      return 'Mohon maaf, uang tidak cukup';
    } else {
      var change = money;
      var items = [
        'Sepatu Stacattu',
        'Baju Zoro',
        'Baju H&N',
        'Sweater Uniklooh',
        'Casing Handphone',
      ];
      var cost = [1500000, 500000, 250000, 175000, 50000];
      var buy = [];

      while (change >= 50000) {
        if (change >= 1500000) {
          if (!buy.includes(items[0])) {
            buy.push(items[0]);
            change = change - cost[0];
          }
        }
        if (change >= 500000) {
          if (!buy.includes(items[1])) {
            buy.push(items[1]);
            change = change - cost[1];
          }
        }
        if (change >= 250000) {
          if (!buy.includes(items[2])) {
            buy.push(items[2]);
            change = change - cost[2];
          }
        }
        if (change >= 175000) {
          if (!buy.includes(items[3])) {
            buy.push(items[3]);
            change = change - cost[3];
          }
        }
        if (change >= 50000) {
          if (!buy.includes(items[4])) {
            buy.push(items[4]);
            change = change - cost[4];
          }
          break;
        }
      }
      var data = {
        memberId: memberId,
        money: money,
        listPurchased: buy,
        changeMoney: change,
      };
      return data;
    }
  }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log('*******************************');

/**
 * Nomor 3
 */
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var arr = [];
  arrPenumpang.forEach((elm) => {
    var dari = elm[1];
    var tujuan = elm[2];
    var harga = Math.abs(
      (dari.charCodeAt(dari.length - 1) -
        tujuan.charCodeAt(tujuan.length - 1)) *
        2000
    );
    arr.push({
      penumpang: elm[0],
      naikDari: dari,
      tujuan: tujuan,
      bayar: harga,
    });
  });
  return arr;
}

//TEST CASE
console.log(
  naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B'],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
