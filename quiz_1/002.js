/**
 * Nomor 1
 */
function DescendingTen(num) {
  if (num == null) {
    return -1;
  } else {
    // Init array
    var ls = [];
    for (var i = num; i > num - 10; i--) {
      ls.push(i);
    }

    // Casting int to string
    ls.toString().split(',');

    return ls;
  }
}
console.log(DescendingTen(100)); // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)); // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()); // -1
console.log('**********************************');

/**
 * Nomor 2
 */
function AscendingTen(num) {
  if (num == null) {
    return -1;
  } else {
    // Init array
    var ls = [];
    for (var i = num; i < num + 10; i++) {
      ls.push(i);
    }

    // Casting int to string
    ls.toString().split(',');

    return ls;
  }
}
console.log(AscendingTen(11)); // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)); // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()); // -1
console.log('**********************************');

/**
 * Nomor 3
 */
function ConditionalAscDesc(reference, check) {
  if (reference == null || check == null) {
    return -1;
  } else {
    check = reference % 2 == 1;
    if (check) {
      return AscendingTen(reference);
    } else {
      return DescendingTen(reference);
    }
  }
}
console.log(ConditionalAscDesc(20, 8)); // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)); // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)); // -1
console.log(ConditionalAscDesc()); // -1
console.log('**********************************');

/**
 * Nomor 4
 */
function ularTangga() {
  var str = '';
  str = str + DescendingTen(100) + '\n';
  str = str + AscendingTen(81) + '\n';
  str = str + DescendingTen(80) + '\n';
  str = str + AscendingTen(61) + '\n';
  str = str + DescendingTen(60) + '\n';
  str = str + AscendingTen(41) + '\n';
  str = str + DescendingTen(40) + '\n';
  str = str + AscendingTen(21) + '\n';
  str = str + DescendingTen(20) + '\n';
  str = str + AscendingTen(1) + '\n';
  return str;
}
console.log(ularTangga());
