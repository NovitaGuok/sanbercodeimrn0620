/**
 * Nomor 1
 */
console.log('LOOPING PERTAMA')
var i = 1
while (i <= 20) {
  if (i % 2 == 0) {
    console.log(i + ' - I love coding')
  }
  i++
}
console.log('LOOPING KEDUA')
var i = 20
while (i >= 1) {
  if (i % 2 == 0) {
    console.log(i + ' - I love coding')
  }
  i--
}
console.log('*****************************')
/**
 * Nomor 2
 */
var i
for (i = 1; i <= 20; i++) {
  if (i % 3 == 0 && i % 2 == 1) {
    console.log(i + ' - I Love Coding')
  } else if (i % 2 == 0) {
    console.log(i + ' - Berkualitas')
  } else if (i % 2 == 1) {
    console.log(i + ' - Santai')
  }
}
console.log('*****************************')
/**
 * Nomor 3
 */
var rows = 4
var cols = 8
for (i = 0; i < rows; i++) {
  var str = ''
}
for (i = 0; i < cols; i++) {
  var str = str + '#'
}
for (i = 0; i < rows; i++) {
  console.log(str)
}

console.log('*****************************')
/**
 * Nomor 4
 */
var i
var str = ''
for (i = 0; i < 1; i++) {
  for (j = 0; j < 7; j++) {
    str = str + '#'
    console.log(str)
  }
}
console.log('*****************************')
/**
 * Nomor 5
 */
var size = 8
var papan = ''

for (var y = 0; y < size; y++) {
  for (var x = 0; x < size; x++) {
    if ((x + y) % 2 == 0) {
      papan += ' '
    } else {
      papan += '#'
    }
  }
  papan += '\n'
}

console.log(papan)
