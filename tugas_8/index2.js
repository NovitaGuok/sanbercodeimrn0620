var readBooksPromise = require('./promise.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
var temp = 10000;
function idxBook(x) {
  if (x == books.length) return 0;
  readBooksPromise(temp, books[x])
    .then(function (fulfilled) {
      temp -= books[x].timeSpent;
      idxBook(x + 1);
      return fulfilled;
    })
    .catch(function (rejected) {
      return rejected;
    });
}
idxBook(0);

// var temp = 10000;
// function idxBook(x) {
//   if (x == books.length) return 0;
//   readBooks(temp, books[x], function (callback) {
//     temp -= books[x].timeSpent;
//     idxBook(x + 1);
//   });
// }
// idxBook(0);
