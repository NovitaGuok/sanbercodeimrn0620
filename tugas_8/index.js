// di index.js
var readBooks = require('./callback.js');

var books = [
  { name: 'LOTR', timeSpent: 3000 },
  { name: 'Fidas', timeSpent: 2000 },
  { name: 'Kalkulus', timeSpent: 4000 },
];

// Tulis code untuk memanggil function readBooks di sini
var temp = 10000;
function idxBook(x) {
  if (x == books.length) return 0;
  readBooks(temp, books[x], function (callback) {
    temp -= books[x].timeSpent;
    idxBook(x + 1);
  });
}
idxBook(0);
